<?php
// --------------> Funcionario <------------------------------------------

// função cadastrar funcionario
function cad_func($conexao,$nome,$email,$criptografa,$categoria, $nome_imagem)
{
		$sql = "INSERT INTO usuario(nome, email, senha, perfil_id, foto) VALUES('{$nome}','{$email}','{$criptografa}','{$categoria}','{$nome_imagem}')";
		$resultado = mysqli_query($conexao, $sql)or die(mysqli_error($conexao));

		return $resultado;
}
// função excluir funcionario

function excluir_func($conexao, $id_usuario)
{
		$sql = "DELETE FROM usuario  WHERE id_usuario = {$id_usuario}";
		$resultado = mysqli_query($conexao, $sql);

		return $resultado;
}

function listar_tipoUsuario($conexao)
{
		$sql = "SELECT id_perfil, nome_perfil FROM perfil";
		$resultado = mysqli_query($conexao, $sql);

		return $resultado;
}


function listar_tipoProduto($conexao)
{
		$sql = "SELECT id_categoria, nome_categoria FROM categoria_produto";
		$resultado = mysqli_query($conexao, $sql);

		return $resultado;
}
// função alterar funcionario

function alterar_func($conexao, $id, $nome, $funcao, $email, $nome_imagem)
{

		$sql = "UPDATE usuario SET nome = '{$nome}', email = '{$email}', foto = '{$nome_imagem}' WHERE id_usuario = {$id}";
		$resultado = mysqli_query($conexao, $sql)or die(mysqli_error($conexao) );

		return $resultado;
}

function alterar_status_pedido($conexao, $num_comanda, $forma_pagamento, $valor_total)
{
		date_default_timezone_set('America/Sao_Paulo');
		$date = date("Y-m-d");
		$sql = "UPDATE pedido_produto SET status = 'Fechado', forma_pagamento = '{$forma_pagamento}', valor_total = '{$valor_total}', data_compra = '{$date}' WHERE num_comanda = {$num_comanda}";
		$resultado = mysqli_query($conexao, $sql)or die(mysqli_error($conexao) );

		return $resultado;
}

// função listar funcionario

function listar_func($conexao)
{
		$sql = "SELECT id_usuario,nome FROM usuario ORDER BY id_usuario";
		$resultado = mysqli_query($conexao, $sql);

		return $resultado;
}



// ----------------------------->Produto <--------------------------------------

function cad_produto($conexao,$nome_produto,$valor_unitario, $categoria, $descricao, $imagem)
{
		$sql = "INSERT INTO produto (nome_produto, valor_unitario, categoria_id, descricao, foto) VALUES ('{$nome_produto}', {$valor_unitario}, {$categoria}, '{$descricao}', '{$imagem}' )";
		$resultado = mysqli_query($conexao, $sql)or die(mysqli_error($conexao));

		return $resultado;
}
function alterar_categoria_produto($conexao, $id_categoria, $nome_categoria)
{
		$sql = "UPDATE categoria_produto SET nome_categoria = '{$nome_categoria}' WHERE id_categoria = {$id_categoria}";
		$resultado = mysqli_query($conexao, $sql)or die(mysqli_error($conexao) );

		return $resultado;
}
function cad_categoria_produto($conexao,$nome_categoria)
{
		$sql = "INSERT INTO categoria_produto (nome_categoria) VALUES ('{$nome_categoria}')";
		$resultado = mysqli_query($conexao, $sql)or die(mysqli_error($conexao));

		return $resultado;
}

function excluir_produto($conexao, $id_produto)
{
		$sql = "DELETE FROM produto  WHERE id_produto = {$id_produto}";
		$resultado = mysqli_query($conexao, $sql);

		return $resultado;
}

function excluir_categ_produto($conexao, $id_categ_produto)
{
		$sql = "DELETE FROM categoria_produto  WHERE id_categoria = {$id_categ_produto}";
		$resultado = mysqli_query($conexao, $sql);

		return $resultado;
}

function alterar_produto($conexao, $id_produto, $nome_produto,$valor_unitario,$tipo_produto, $nome_imagem, $descricao)
{

		$sql = "UPDATE produto SET nome_produto = '{$nome_produto}', valor_unitario = {$valor_unitario}, categoria_id = {$tipo_produto}, foto = '{$nome_imagem}', descricao = '{$descricao}'  WHERE id_produto = {$id_produto}";
		$resultado = mysqli_query($conexao, $sql)or die(mysqli_error($conexao) );

		return $resultado;
}
function listar_produto($conexao)
{
		$sql = "SELECT id_produto, nome_produto, valor_unitario, descricao, foto FROM produto ORDER BY id_produto";
		$resultado = mysqli_query($conexao, $sql);

		return $resultado;
}

// ----------------------------> Fazer Pedido <--------------------------------

function selecionar_produto($conexao,$categoria){
	$sql = "SELECT id_produto, nome_produto, valor_unitario, descricao, foto FROM produto WHERE '{$categoria}' = categoria_id";
	$resultado = mysqli_query($conexao, $sql)or die(mysqli_error($conexao));

	return $resultado;

}

function salvar_pedidos($conexao,$num_comanda,$produto_id, $Qtd, $status){
	$sql = "INSERT INTO pedido_produto (produto_id, num_comanda, quantidade, status) VALUES ('{$produto_id}', '{$num_comanda}', '{$Qtd}', '{$status}')";
	$resultado = mysqli_query($conexao, $sql)or die(mysqli_error($conexao));

	return $resultado;

}

function verificaUsuario($conexao, $email)
{
	$sql = "SELECT email,nome FROM usuario WHERE email = '{$email}'";
	$resultado = mysqli_query($conexao, $sql)or die(mysqli_error($conexao));

	return $resultado;
}


//------------------------------Select Login--------------------------------------------
function selecionaUsuario($conexao, $email, $criptografa)
{
	$sql = "SELECT id_usuario, nome, email, senha, perfil_id, foto FROM usuario WHERE email = '{$email}' AND senha = '{$criptografa}'";
	$resultado = mysqli_query($conexao, $sql);

	return $resultado;
}

function inserir_pedido($conexao, $id, $num)
{
	$sql = "INSERT INTO pedido(id_pedido, num_comanda), pedido_produto(pedido_id, produto_id) VALUES('{$id}','{$num}'),('{}','{$id}')";
	$resultado = mysqli_query($conexao, $sql);

	return $resultado;
}

//---------------------------------Altera Senha---------------------------------------------

function altera_senha($conexao, $email, $criptografa)
{
	$sql = "UPDATE usuario SET senha = '{$criptografa}' WHERE email = '{$email}'";
	$resultado = mysqli_query($conexao, $sql);

	return $resultado;
}

//--------------------------------Listar Comanda----------------------------------

function listar_pedidos($conexao, $comanda, $status)
{
	$sql = "SELECT * FROM pedido_produto INNER JOIN produto WHERE num_comanda = '{$comanda}' AND produto_id = id_produto AND status = '{$status}'";
	$resultado = mysqli_query($conexao, $sql);

	return $resultado;
}

//---------------------------------Alterar Pedido--------------------------------------

function alterar_pedido($conexao, $id_produto, $nome_produto,$valor_unitario,$tipo_produto, $nome_imagem, $descricao)
{

		$sql = "UPDATE produto SET nome_produto = '{$nome_produto}', valor_unitario = {$valor_unitario}, categoria_id = {$tipo_produto}, foto = '{$nome_imagem}', descricao = '{$descricao}'  WHERE id_produto = {$id_produto}";
		$resultado = mysqli_query($conexao, $sql)or die(mysqli_error($conexao) );

		return $resultado;
}

function listar_pedidosFechado($conexao, $status)
{
	$sql = "SELECT * FROM pedido_produto INNER JOIN produto WHERE  produto_id = id_produto AND status = '{$status}'";
	$resultado = mysqli_query($conexao, $sql);

	return $resultado;
}

function listar_pedidosAberto($conexao, $status)
{
	$sql = "SELECT * FROM pedido_produto INNER JOIN produto WHERE produto_id = id_produto AND status = '{$status}'";
	$resultado = mysqli_query($conexao, $sql);

	return $resultado;
}
//---------------------------------Altera Pedido------------------------------------
function altera_pdd($conexao,$id_pedido_produto,$quantidade)
{

		$sql = "UPDATE pedido_produto SET quantidade = '{$quantidade}'  WHERE id_pedido_produto = {$id_pedido_produto}";
		$resultado = mysqli_query($conexao, $sql)or die(mysqli_error($conexao) );

		return $resultado;
}

function canc_pedido($conexao, $id_pedido_produto, $status)
{
		$sql = "UPDATE pedido_produto SET status = '{$status}'  WHERE id_pedido_produto = {$id_pedido_produto}";
		$resultado = mysqli_query($conexao, $sql);

		return $resultado;
}

//-------------------------------------Selecionando pedido-------------------

/*function selec_pedido_alt($conexao, $id_pedido_produto)
{
	$sql = "SELECT * FROM pedido_produto INNER JOIN produto WHERE id_pedido_produto = '{$id_pedido_produto}' AND id_produto = produto_id";
	$resultado = mysqli_query($conexao, $sql);

	return $resultado;
}*/


?>
