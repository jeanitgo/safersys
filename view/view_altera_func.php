<?php

  ob_start();

require_once("../model/conexao.php");
require_once("../model/model_funcao.php");
if (@$_SESSION['perfil_id'] == 2 || @$_SESSION['perfil_id'] == 3) {
    # code...
    session_destroy();
  // Redireciona o visitante de volta pro login
    header("Location: ../index.php"); 
}
$a = listar_tipoUsuario($conexao);


/**
* Função para salvar mensagens de LOG no MySQL
*
* @param string $mensagem - A mensagem a ser salva
*
* @return bool - Se a mensagem foi salva ou não (true/false)
*/


/*function salvaLog($mensagem) {
    $ip = $_SERVER['REMOTE_ADDR']; // Salva o IP do visitante
    //$nome = $_SESSION['nome'];
    $hora = date('Y-m-d H:i:s'); // Salva a data e hora atual (formato MySQL)
    // Usamos o mysql_escape_string() para poder inserir a mensagem no banco
    //   sem ter problemas com aspas e outros caracteres
    $mensagem = mysql_escape_string($mensagem);
    // Monta a query para inserir o log no sistema
    $sql = "INSERT INTO log(data_hora, mensagem) VALUES ('{$hora}','{$mensagem}')";
    if (mysql_query($sql)) {
    echo "hello";
    } else {
    echo "erro";
    }
}*/


?>
<style type="text/javascript">
    function validar(){
        
    }
    
</style>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>
                    Altera Usuário
                </h2>
                <div class="clearfix">
                </div>
            </div>
            <div class="x_content">
              <?php require('../control/altera_funcionario.php'); ?>
                <form class="form-horizontal form-label-left" method="POST" enctype="multipart/form-data" action="../control/altera_funcionario.php" onsubmit="return validar_usuario();" novalidate
                id="formFunc" data-toggle="validator" role="form">
                    
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                            Nome
                            <span class="required">
                                *
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" id="nome" name="nome" maxlength="45"
                            required="required" value="<?=$resultados['nome']?>">
                            <div class="help-block with-errors"></div>
                        </div>
                       
                    </div>
                   
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">
                            Email
                            <span class="required">
                                *
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="email" id="email" name="email" required="required" class="form-control col-md-7 col-xs-12" value="<?=$resultados['email']?>">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="website">
                            Ocupação
                            <span class="required">
                                *
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control" name="ocupacao" id="cbValores">
                                <option value="0">--Selecione--</option>
                                <?php foreach ($a as $key) { ?>
                                    <option value="<?=$key['id_perfil']?>"><?=$key['nome_perfil']?></option>
                                <?php }?>
                                
                            </select>
                        </div>
                    </div>
                     <div class="item form-group">
                        <label for="password2" class="control-label col-md-3 col-sm-3 col-xs-12">
                            Selecione a foto
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="imagem" type="file" name="imagem" value="<?=$resultados['foto']?>"
                            class="form-control col-md-7 col-xs-12" required="required">
                            
                        </div>
                    </div>
  
                    <div class="ln_solid">
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                                    
                    <div class="btn-group">
                        <input type="hidden" name="frmAlterar" value="<?=$resultados['id_usuario']?>">
                        <button type="submit" class="btn btn-primary">Alterar</button>
                        <?php 
                       /*     if (@$_POST['frmAlterar']) {
                                # code...
                                 $mensagem = "alteração de usuario";
                                 $salvaLog($mensagem);
                            }
                             */
                        ?>
                    </div>
                    <div class="btn-group">
                        <a href="../view/view_listar_func.php"><button type="button" class="btn btn-primary">Voltar</button></a>  
                    </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php 

$pagemaincontent = ob_get_contents();
ob_end_clean(); 

$pagetitle = "Alterar Funcionario";

include("master.php");


?>        