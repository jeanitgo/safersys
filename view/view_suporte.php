<?php 

  ob_start();
/*$nivel_necessario = 2;*/
// Verifica se não há a variável da sessão que identifica o usuário
/*if (!isset($_SESSION['UsuarioId']) OR ($_SESSION['UsuarioNivel'] < $nivel_necessario)) {
  // Destrói a sessão por segurança
  session_destroy();
  // Redireciona o visitante de volta pro login
  header("Location: ../index.php"); exit;*/
//}
?>
        <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>
                    Suporte
                </h2>
                <div class="clearfix">
                </div>
            </div>
            <div class="x_content">
                <form class="form-horizontal form-label-left" action="../control/email.php" method="POST" novalidate>
                    
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                            Nome
                            <span class="required">
                                *
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" id="nome" name="nome" maxlength="45"
                            required="required">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">
                            E-mail
                            <span class="required">
                                *
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">  
                            <input type="email" id="email" name="email" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                        
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">
                            Assunto
                            <span class="required">
                                *
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" id="assunto" name="assunto" maxlength="45">
                        </div>
                    </div>
                     <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">
                            Mensagem
                            <span class="required">
                                *
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea class="form-control"name="descricao" id="" cols="30" rows="5"></textarea>
                        </div>
                    </div>
                    
                    <div class="ln_solid">
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <input type="hidden" name="frmCadUsuario">
                            <button type="submit" class="btn btn-primary">
                                Enviar
                            </button>
                            <button type="reset" class="btn btn-warning">
                                Limpar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

  <?php 

$pagemaincontent = ob_get_contents();
ob_end_clean(); 

$pagetitle = "Suporte";

include("master.php");

?>   