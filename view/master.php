<?php
// A sessão precisa ser iniciada em cada página diferente
if (!isset($_SESSION)) session_start();
// Verifica se não há a variável da sessão que identifica o usuário
if (!isset($_SESSION['nome'])) {
  // Destrói a sessão por segurança
  session_destroy();
  // Redireciona o visitante de volta pro login
  header("Location: ../index.php"); 
}

$nivelAdm = 1;
$nivelGarcom = 2; 
$nivelCaixa = 3;

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title><?php echo $pagetitle ?></title>
        <!-- Bootstrap -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">
        <link rel="shortcut icon" href="../imagens/icon2.png" >
        <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="css/animate.min.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            <a href="/proj-ss/view/view_inicial.php" class="site_title"><img src="/proj-ss/imagens/icon1.png" alt=""> <span>Comandas</span></a>
                        </div>
                        <div class="clearfix">
                        </div>
                        <!-- /menu prile quick info -->
                        <br />
                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                            <div class="menu_section">
                                <ul class="nav side-menu">
                                    <li><a href="/proj-ss/view/view_inicial.php"><i class="fa fa-home"></i> Home</a>
                                    <?php if ($_SESSION['perfil_id'] == $nivelAdm || $_SESSION['perfil_id'] == 11 ) { ?>
                                       
                                     
                                    <li>
                                        <a><i class="fa fa-user-plus"></i> Usuário <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">
                                            <li>
                                                <a href="/proj-ss/view/view_funcionario.php">Cadastrar</a>
                                            </li>
                                            <li>
                                                <a href="/proj-ss/view/view_listar_func.php">Listar</a>
                                            </li>

                                        </ul>
                                    </li>
                                    <li>
                                        <a><i class="fa fa-cutlery"></i> Produto <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">
                                            <li>
                                                <a href="/proj-ss/view/view_produto.php">Cadastrar</a>
                                            </li>
                                            <li>
                                                <a href="/proj-ss/view/view_listar_prod.php">Listar</a>
                                            </li>

                                        </ul>
                                    </li>
                                     
                                         
                                     
                                    <li>
                                        <a href="/proj-ss/view/view_categoria_produto.php"><i class="fa fa-plus-circle"></i> Adicionar Categoria <span class="fa fa-chevron-down"></span></a>
                                       
                                    </li>
                                    
                                    <?php }if($_SESSION['perfil_id'] == $nivelAdm  || $_SESSION['perfil_id'] == $nivelGarcom || $_SESSION['perfil_id'] == 11) { ?>
                                   
                                    <li>
                                        <a><i class="fa fa-cart-plus"></i> Pedidos <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">
                                            <li>
                                                <a href="\proj-ss\view\view_fazer_pedido.php">Novo Pedido</a>
                                            </li>
                                            <li>
                                                <a href="\proj-ss\view\view_pedidos.php">Listar</a>
                                            </li>

                                        </ul>
                                    </li>
                                    <?php } if ($_SESSION['perfil_id'] == $nivelAdm  || $_SESSION['perfil_id'] == $nivelCaixa || $_SESSION['perfil_id'] == 11) { ?>
                                        
                                     <li>
                                        <a href="\proj-ss\view\view_caixa.php"><i class="fa fa-pencil-square-o"></i> Caixa <span class="fa fa-chevron-down"></span></a>
                                    </li>
                                    
                                 <?php } ?>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- top navigation -->
                <div class="top_nav">
                    <div class="nav_menu">
                        <nav class="" role="navigation">
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>
                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                                        aria-expanded="false">
                                 
                                        <img src="../imagens/<?=$_SESSION['foto']?>" alt=""><?=$_SESSION['nome']?>
                                  

                                    <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                        <li>
                                            <a href="/proj-ss/index.php"><i class="fa fa-sign-out pull-right"></i>Sair</a>
                                                
                                        </li>
                                         <li>
                                            <a href="view_altera_senha.php"><i class="fa fa-key pull-right"></i>Alterar Senha</a>
                                        </li>
                                    </ul>
                                </li>
                                <li role="presentation" class="dropdown">
                                    <a href="\proj-ss\view\view_suporte.php"
                                    <i class="fa fa-envelope-o"></i>

                                    </a>

                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="right_col" role="main">
                <?php echo $pagemaincontent; ?>
            </div>
        </div>
        <!-- /menu footer buttons -->
                   <!--  <div class="sidebar-footer hidden-small">
                       <img class="img-responsive img-rounded" src="..\imagens\modelo1.png" alt="logo">
                   </div> -->
                    <!-- /menu footer buttons -->
                </div>
        <!-- /top navigation -->
        <!-- Placed at the end of the document so the pages load faster -->
        
        <script src="../js/jquery.min.js"></script>
        <script src="../js/jquery.mask.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/script.js"></script>
        <script src="../js/pedido.js"></script>
        <script src="../js/caixa.js"></script>
         
          <script src="../js/validator.js"></script>
          <script src="../js/validator.min.js"></script>


    </body>
</html>
