<?php

  ob_start();
if (@$_SESSION['perfil_id'] == 2 || @$_SESSION['perfil_id'] == 3) {
    # code...
    session_destroy();
  // Redireciona o visitante de volta pro login
    header("Location: ../index.php"); 
}

if(@$_GET['status'] == 'sucesso')
{ ?>
  <div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="alert alert-success" role="alert">
        <strong>Adicionado com sucesso!</strong>
      </div>
    </div>
  </div>

<?php
}

if(@$_GET['status'] == 'erro')
{ ?>
  <div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="alert alert-danger" role="alert">
        <strong>Ocorreu um erro, tente novamente!</strong>
      </div>
    </div>
  </div>
<?php
}
?>
<div class="col-md-6 col-sm-6 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Produtos <small>somente administrador tem acesso</small></h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <?php require('../control/listar_produto.php'); ?>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nome</th>
                        <th>Valor</th>
                        <th>Ação</th>
                        
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data as $resultados) { ?>
                      <tr>
                        <th><?=$resultados['id_produto']?></th>
                        <th><?=$resultados['nome_produto']?></th>
                        <th>R$<?=$resultados['valor_unitario']?></th>
                        <th><a href="../view/view_vizualiza_prod.php?codigo=<?=$resultados['id_produto']?>"><i class="fa fa-eye fa-3x" aria-hidden="true"></i></a>
                        <a href="../view/view_altera_prod.php?codigo=<?=$resultados['id_produto']?>"><i class="fa fa-pencil-square-o fa-3x" aria-hidden="true"></i></a>
                        <a href="../control/excluir_produto.php?codigo=<?=$resultados['id_produto']?>" onclick="return confirm('Deseja realmente excluir?')"><i class="fa fa-ban fa-3x" aria-hidden="true"></i></a></th>
                      </tr>
                    <?php } ?>

                </tbody>
            </table>
        </div>
    </div>
</div>
<?php

$pagemaincontent = ob_get_contents();
ob_end_clean();

$pagetitle = "Lista Produto";

include("master.php");


?>
