<?php

  ob_start();

?>	
 
	<!-- <img class="img-responsive img-rounded" src="..\imagens\modelo1.png" alt="logo"> -->
    <!-- top tiles -->
    <div class="" role="main">

                <br />
                <div class="">

                    <div class="row top_tiles">
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-users"></i>
                                </div>
                                <div class="count">10</div>

                                <h3>Clientes</h3>
                                <p>Total de cliente do dia</p>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-cutlery"></i>
                                </div>
                                <div class="count">2</div>

                                <h3>Preparando</h3>
                                <p>Em andamento</p>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-hourglass-start"></i>
                                </div>
                                <div class="count">8</div>

                                <h3>Aguardando</h3>
                                <p>Em espera</p>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-check-square-o"></i>
                                </div>
                                <div class="count">10</div>

                                <h3>Pedidos</h3>
                                <p>Total de pedidos do dia</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
               

<?php

$pagemaincontent = ob_get_contents();
ob_end_clean();

$pagetitle = "Inicial";

include("master.php");


?>
