<?php

  ob_start();
if (@$_SESSION['perfil_id'] == 2 || @$_SESSION['perfil_id'] == 3) {
    # code...
    session_destroy();
  // Redireciona o visitante de volta pro login
    header("Location: ../index.php"); 
}
?>
<style type="text/javascript">
    function validar(){
        
    }
    
</style>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>
                    Listar Usuário
                </h2>
                <div class="clearfix">
                </div>
            </div>
            <div class="x_content">
              <?php require('../control/altera_funcionario.php'); ?>
                <form class="form-horizontal form-label-left" method="POST" action="../control/altera_funcionario.php" onsubmit="return validar_usuario();" novalidate>
                    
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                            Nome
                            <span class="required">
                                *
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" id="nome" name="nome" maxlength="45"
                            required="required" value="<?=$resultados['nome']?>" readonly>
                        </div>
                       
                    </div>
                   
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">
                            Email
                            <span class="required">
                                *
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="email" id="email" name="email" required="required" readonly class="form-control col-md-7 col-xs-12" value="<?=$resultados['email']?>">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="website">
                            Ocupação
                            <span class="required">
                                *
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" id="nome" name="select" value="<?=$resultados['nome_perfil']?>" readonly maxlength="45">
                        </div>
                    </div>
                     <div class="item form-group">
                        <center>
                            <img  src="../imagens/<?=$resultados['foto']?>" align="center" class="img-responsive img-thumbnail" width="40%" alt="Responsive image">
                        </center>
                    </div>
  
                    <div class="ln_solid">
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="btn-group">
                              <a href="../view/view_listar_func.php"><button type="button" class="btn btn-primary">Voltar</button></a>
                           </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php 

$pagemaincontent = ob_get_contents();
ob_end_clean(); 

$pagetitle = "Funcionario";

include("master.php");

?>       