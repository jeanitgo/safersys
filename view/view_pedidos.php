<?php

  ob_start();

require_once("../model/conexao.php");
require_once("../model/model_funcao.php");
 $data = array();
 $comanda = null;

if ($_POST) {
    # code...
  $comanda = $_POST['nmComanda'];
  $status = "Aberto";
  $a = listar_pedidos($conexao, $comanda, $status);
//var_dump($a);
    # code...
    while($row = mysqli_fetch_array($a))
    {

      $data[] = array("id_pedido_produto" => $row['id_pedido_produto'],"nome_produto" => $row['nome_produto'],"num_comanda" => $row['num_comanda'],"quantidade" => $row['quantidade']);

    }
}
if(isset($_GET['id']) == 1){
//echo"1";
  $status = "Fechado";
  $a = listar_pedidosFechado($conexao, $status);

   while($row = mysqli_fetch_array($a))
    {
          # code...
      $data[] = array("id_pedido_produto" => $row['id_pedido_produto'],"nome_produto" => $row['nome_produto'],"num_comanda" => $row['num_comanda'],"quantidade" => $row['quantidade']);
          //var_dump($data);
    }

  # code...
}if(isset($_GET['ap']) == 2) {

  $status = "Aberto";
  $a = listar_pedidosAberto($conexao, $status);

   while($row = mysqli_fetch_array($a))
    {
          # code...
      $data[] = array("id_pedido_produto" => $row['id_pedido_produto'],"nome_produto" => $row['nome_produto'],"num_comanda" => $row['num_comanda'],"quantidade" => $row['quantidade']);

    }
}
?>
<div class="col-md-6 col-sm-6 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>
                Lista de Pedidos
                <small>
                    Aqui você pode cancelar um pedido ou alterar.
                </small>
            </h2>
            <ul class="nav navbar-right panel_toolbox">
                <li>
                    <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
            </ul>
             <div class="title_right">
                          <form action="" method="POST">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" name="nmComanda" class="form-control">
                                    <span class="input-group-btn">
                                        <input type="hidden" name="busca">             
                                        
                                        <button class="btn btn-default" type="submit">Buscar</button>
                                    </span>
                                </div>
                            </div>
                          </form>
                        

             </div>

                          <div style="float: left">
                            <form action="" method="GET">
                              <div class="col-md-5 col-sm-5 col-xs-12 ">
                                  <div class="input-group">
                                          <a class="btn btn-default" href="view_pedidos.php?id=1">Fechado</a>
                                  </div>
                              </div>
                            </form>
                          </div>
                          <div style="float: left">
                            <form action="" method="GET">
                              <div class="col-md-5 col-sm-5 col-xs-12 ">
                                  <div class="input-group">
                                          <a class="btn btn-default" href="view_pedidos.php?ap=2">Aberto</a>
                                  </div>
                              </div>
                            </form>
                          </div>

            <div class="clearfix">
            </div>
        </div>
        <div class="x_content">
                
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th style="width: 80px;">
                              Comanda
                            </th>
                            <th>
                                Produto
                            </th>
                            <th>
                                Qtd
                            </th>
                            <th >
                                Ação
                            </th>
                             
                        </tr>
                    </thead>
                    <tbody>
                      
                      <?php foreach ($data as $resultados) { ?>
                       <tr>
                                <th><?=$resultados['id_pedido_produto']?></th>
                                 <th><?=$resultados['num_comanda']?></th>
                                <th><?=$resultados['nome_produto']?></th>
                                 <th><?=$resultados['quantidade']?></th>
                                <th><a href="view_altera_pedido.php?codigo=<?=$resultados['id_pedido_produto']?>"><i class="fa fa-pencil-square-o fa-3x" aria-hidden="true"></a></i>
                                  <a href="../control/cancelar_pedido.php?codigo=<?=$resultados['id_pedido_produto']?>" value="<?=$resultados['id_pedido_produto']?>"><i class="fa fa-ban fa-3x" aria-hidden="true"></i></a></th>
                        </tr>        
                       <?php } ?>
                    </tbody>
                </table>
        </div>
    </div>
</div>
<?php

$pagemaincontent = ob_get_contents();
ob_end_clean();

$pagetitle = "Lista Produto";

include("master.php");


?>                        