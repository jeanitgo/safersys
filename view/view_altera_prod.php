<?php

ob_start();
require('../control/altera_produto.php');
require('../model/conexao.php');

if (@$_SESSION['perfil_id'] == 2 || @$_SESSION['perfil_id'] == 3) {
    # code...
    session_destroy();
  // Redireciona o visitante de volta pro login
    header("Location: ../index.php"); 
}
$b = listar_tipoProduto($conexao);

?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>
                    Alterar Produto
                </h2>
                <div class="clearfix">
                </div>
            </div>
            <div class="x_content">
                <form class="form-horizontal form-label-left" enctype="multipart/form-data"  action="../control/altera_produto.php" method="POST" novalidate
                id="formAltProd" data-toggle="validator" role="form">
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                            Nome
                            <span class="required">
                                *
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" id="nome" name="nomeProduto" maxlength="45"
                            required="required" value="<?=$resultados['nome_produto']?>" >
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">
                            Valor Unitario
                            <span class="required">
                                *
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">


                         <div class="input-group">
                          <div class="input-group-addon">R$</div>
                           <input type="text" class="form-control" id="field" name="valorUnitario" id="mask-moeda" value="<?=$resultados['valor_unitario']?>"  onkeyup="maskMoeda(this.id);" required="required">
                            <div class="input-group-addon">.00</div>
                          </div>
                        </div>

                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">
                            Categoria Produto
                            <span class="required">
                                *
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <select name="tipo_produto" required class="form-control" id="">
                              <option value="0" selected>--Selecione--</option>
                              <?php foreach ($b as $key) { ?>
                                    <option value="<?=$key['id_categoria']?>"><?=$key['nome_categoria']?></option>
                              <?php }?>

                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label for="password2" class="control-label col-md-3 col-sm-3 col-xs-12">
                            Selecione a imagem
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                             <input id="imagem" type="file" name="imagem" class="form-control col-md-7 col-xs-12" required="required">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">
                            Descrição
                            <span class="required">

                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea class="form-control" id="" cols="30" name="descricao" rows="5" ><?=$resultados['descricao']?></textarea>
                        </div>
                    </div>

                    <div class="ln_solid">
                    </div>
                   <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <input type="hidden" name="frmCadUsuario">
                            <button type="submit" class="btn btn-primary">
                                <input type="hidden" name="frmAlterar"  value="<?=$resultados['id_produto']?>">
                               Alterar
                            </button>
                             <a href="../view/view_listar_prod.php"><button type="button" class="btn btn-primary">Voltar</button></a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<?php

$pagemaincontent = ob_get_contents();
ob_end_clean();

$pagetitle = "Produtos";

include("master.php");


?>
