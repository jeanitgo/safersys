<?php

ob_start();
require('../control/altera_produto.php');
require('../model/conexao.php');
if (@$_SESSION['perfil_id'] == 2  AND @$_SESSION['perfil_id'] == 3) {
    # code...
    session_destroy();
  // Redireciona o visitante de volta pro login
    header("Location: ../index.php"); 
}
?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>
                    Listar Produto
                </h2>
                <div class="clearfix">
                </div>
            </div>
            <div class="x_content">
                <form class="form-horizontal form-label-left" novalidate>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                            Nome
                            <span class="required">
                                *
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" id="nome" name="nome_produto" maxlength="45"
                            required="required" value="<?=$resultados['nome_produto']?>" readonly>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">
                            Valor Unitario
                            <span class="required">
                                *
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">


                         <div class="input-group">
                          <div class="input-group-addon">R$</div>
                           <input type="text" class="form-control" id="field" id="mask-moeda" value="<?=$resultados['valor_unitario']?>" readonly onkeyup="maskMoeda(this.id);" required="required">
                            <div class="input-group-addon">.00</div>
                          </div>
                        </div>

                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">
                            Categoria Produto
                            <span class="required">
                                *
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <input type="text" class="form-control" id="nome" name="select" value="<?=$resultados['nome_categoria']?>" maxlength="45" readonly required="required">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label for="password2" class="control-label col-md-3 col-sm-3 col-xs-12">
                            Selecione a imagem
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="imagem" type="text" value="<?=$resultados['foto']?>" name="imagem" class="form-control col-md-7 col-xs-12" readonly required="required">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">
                            Descrição
                            <span class="required">

                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea class="form-control" id="" cols="30" rows="5" readonly><?=$resultados['descricao']?></textarea>
                        </div>
                    </div>

                    <div class="ln_solid">
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <input type="hidden" name="frmCadUsuario">


                                 <div class="btn-group">
                <a href="../view/view_listar_prod.php"><button type="button" class="btn btn-primary">Voltar</button></a>
            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<?php

$pagemaincontent = ob_get_contents();
ob_end_clean();

$pagetitle = "Produtos";

include("master.php");


?>
