<?php

  ob_start();
require_once("../model/conexao.php");
require_once("../model/model_funcao.php");
if (@$_SESSION['perfil_id'] == 2 || @$_SESSION['perfil_id'] == 3) {
    # code...
    session_destroy();
  // Redireciona o visitante de volta pro login
    header("Location: ../index.php"); 
}
$b = listar_tipoProduto($conexao);

if(@$_GET['status'] == 'sucesso')
{ ?>
  <div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="alert alert-success" role="alert">
        <strong>Categoria Alterado com sucesso!</strong>
      </div>
    </div>
  </div>

<?php
}

if(@$_GET['status'] == 'erro')
{ ?>
  <div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="alert alert-danger" role="alert">
        <strong>Ocorreu um erro, tente novamente!</strong>
      </div>
    </div>
  </div>
<?php
}


if(@$_GET['msg'] == 'sucesso')
{ ?>
  <div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="alert alert-success" role="alert">
        <strong>Adicionado com sucesso!</strong>
      </div>
    </div>
  </div>

<?php
}

if(@$_GET['msg'] == 'erro')
{ ?>
  <div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="alert alert-danger" role="alert">
        <strong>Ocorreu um erro, tente novamente!</strong>
      </div>
    </div>
  </div>
<?php
}


?>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>
                    Nova Categoria
                </h2>
                <div class="clearfix">
                </div>
            </div>
            <div class="x_content">
                <form class="form-horizontal form-label-left" method="POST" action="../control/cad_categoria_produto.php" novalidate>
                    
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                            Nome da categoria
                            <span class="required">
                                *
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" id="nome" name="categoriaProduto" maxlength="30"
                            required="required">
                        </div>
                        
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="frmCadCategoria">
                        <button type="submit" class="btn btn-primary">
                                Adicionar
                        </button>
                    </div>
                    
                    <div class="ln_solid">
                    </div>
                </form>
                <div class="x_content">
                                    <?php require('../control/categoria_produto.php'); ?>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Categoria</th>
                                                <th>Ação</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($data as $resultados) { ?>
                                              <tr>
                                                <th><?=$resultados['id_categoria']?></th>
                                                <th><?=$resultados['nome_categoria']?></th>
                                                <th><a href="../view/view_altera_categoria_produto.php?codigo=<?=$resultados['id_categoria']?>"><i class="fa fa-pencil-square-o fa-3x" aria-hidden="true"></i></a>
                                                <a href="../control/excluir_categoria_produto.php?codigo=<?=$resultados['id_categoria']?>" onclick="return confirm('Deseja realmente excluir?')"><i class="fa fa-ban fa-3x" aria-hidden="true"></i></a></th>
                                              </tr>
                                            <?php } ?>        
                                        
                                        </tbody>
                                    </table>

                                </div>
            </div>
        </div>
    </div>
</div>


<?php

$pagemaincontent = ob_get_contents();
ob_end_clean();

$pagetitle = "Produtos";

include("master.php");


?>