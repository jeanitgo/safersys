<?php

ob_start();
require_once("../model/conexao.php");
require_once("../model/model_funcao.php");
//require_once("../control/login.php");


//$id = $_SESSION['id_usuario'];
?>
<style type="text/javascript">
    function validar(){
        
    }
    
</style>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>
                    Alterar Senha
                </h2>
                <div class="clearfix">
                </div>
            </div>
            <div class="x_content">
                <form class="form-horizontal form-label-left" enctype="multipart/form-data" method="POST" action="..\control\altera_senha.php" onsubmit="return validar_usuario();" novalidate 
                id="formFunc" data-toggle="validator" role="form">
                     <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">
                            Email
                            <span class="required">
                                *
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="email" id="email" name="email" required="required" class="form-control col-md-7 col-xs-12" >
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>       
                    <div class="item form-group">
                        <label for="inputPassword" class="control-label col-md-3">
                            Novo Password
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="senha" name="senha_novo" type="password" data-validate-length="password"
                            class="form-control col-md-7 col-xs-12"
                            data-minlength="6" required>
                            <span class="help-block"> Mínimo de seis (6) digitos </span>
                            
                        </div>
                    </div>
                    <div class="item form-group">
                        <label for="password2" class="control-label col-md-3 col-sm-3 col-xs-12">
                            Repetir Password
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="senha2" type="password" name="senha_repetir" data-validate-linked="password"
                            class="form-control col-md-7 col-xs-12"
                            data-match="#senha" data-match-error="Atenção! As senhas não estão iguais." required>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="ln_solid">
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <?php  ?>
                            <input type="hidden" name="frmAltSenha" value="">
                            <button type="submit" class="btn btn-primary">
                                Alterar
                            </button>
                        
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<?php

$pagemaincontent = ob_get_contents();
ob_end_clean();

$pagetitle = "Funcionarios";

include("master.php");


?>