<?php

ob_start();
require('../control/altera_pedido.php');
require('../model/conexao.php');

if (@$_SESSION['perfil_id'] == 2 || @$_SESSION['perfil_id'] == 3) {
    # code...
    session_destroy();
  // Redireciona o visitante de volta pro login
    header("Location: ../index.php"); 
}
//$b = selec_pedido_alt($conexao, $id_pedido_produto);

?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>
                    Alterar Pedido
                </h2>
                <div class="clearfix">
                </div>
            </div>
            <div class="animated flipInY col-lg-2 col-md-3 col-sm-4 col-xs-12">
                <div class="tile-stats">
                    <div class="right">
                      <form action="../control/altera_pedido.php" method="POST">  
                        <img  src="../imagens/<?=$resultados['foto']?>" class="img-responsive img-thumbnail" width="100%" alt="Responsive image">
                        <h4 name="nome_produto"align="center"><?=$resultados['nome_produto']?></h4>
                        <h5 name="valor_unitario"align="center">R$<?=$resultados['valor_unitario']?></h5>
                        <div class="form-group">
                        <center><input type="number" class="teste" name="quantidade" min="1" max="10" value="<?=$resultados['quantidade']?>"></center>
                        </div>
                            <div class="form-group" align="center">
                                <input type="hidden" class="id" value="">
                                <!-- <a href="#" class="btn btn-success">Alterar</a> -->
                                <button type="submit" class="btn btn-success">
                                <input type="hidden" name="frmPedidoAlt" value="<?=$resultados['id_pedido_produto']?>">
                                Alterar
                                </button>
                                <a href="view_pedidos.php" class="btn btn-primary">Voltar</a>
                            </div>
                            <div class="form-group">    
                                
                            </div>
                      </form>       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php

$pagemaincontent = ob_get_contents();
ob_end_clean();

$pagetitle = "Produtos";

include("master.php");


?>
