<?php

ob_start();
require_once("../model/conexao.php");
require_once("../model/model_funcao.php");
if (@$_SESSION['perfil_id'] == 2 || @$_SESSION['perfil_id'] == 3) {
    # code...
    session_destroy();
  // Redireciona o visitante de volta pro login
    header("Location: ../index.php"); 
}
$a = listar_tipoUsuario($conexao);

?>
<style type="text/javascript">
    function validar(){
        
    }
    
</style>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>
                    Cadastrar Usuário
                </h2>
                <div class="clearfix">
                </div>
            </div>
            <div class="x_content">
                <form class="form-horizontal form-label-left" enctype="multipart/form-data" method="POST" action="..\control\cad_funcionario.php" onsubmit="return validar_usuario();" novalidate 
                id="formFunc" data-toggle="validator" role="form">
                    
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                            Nome
                            <span class="required">
                                *
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" id="nome" name="nome" maxlength="45"
                            required="required">
                            <div class="help-block with-errors"></div>
                        </div>
                       
                    </div>
                   
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">
                            Email
                            <span class="required">
                                *
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="email" id="email" name="email" required="required" class="form-control col-md-7 col-xs-12">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">
                            Confirm Email
                            <span class="required">
                                *
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="email" id="email2" name="confirm_email" data-validate-linked="email"
                            required="required" class="form-control col-md-7 col-xs-12"
                            data-match="#email" data-match-error="Atenção! Os emails não estão iguais." required>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="website">
                            Ocupação
                            <span class="required">
                                *
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control" name="categoria" id="">
                                <option value="0">--Selecione--</option>
                                <?php foreach ($a as $key) { ?>
                                    <option value="<?=$key['id_perfil']?>"><?=$key['nome_perfil']?></option>
                                <?php }?>
                            </select>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label for="inputPassword" class="control-label col-md-3">
                            Password
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="senha" name="senha" type="password" data-validate-length="password"
                            class="form-control col-md-7 col-xs-12"
                            data-minlength="6" required>
                            <span class="help-block"> Mínimo de seis (6) digitos </span>
                            
                        </div>
                    </div>
                    <div class="item form-group">
                        <label for="password2" class="control-label col-md-3 col-sm-3 col-xs-12">
                            Repetir Password
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="senha2" type="password" name="senha2" data-validate-linked="password"
                            class="form-control col-md-7 col-xs-12"
                            data-match="#senha" data-match-error="Atenção! As senhas não estão iguais." required>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label for="password2" class="control-label col-md-3 col-sm-3 col-xs-12">
                            Selecione a foto
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="imagem" type="file" name="imagem"
                            class="form-control col-md-7 col-xs-12" required="required">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="ln_solid">
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <input type="hidden" name="frmCadUsuario">
                            <button type="submit" class="btn btn-primary">
                                Cadastrar
                            </button>
                            <button type="reset" class="btn btn-warning">
                                Limpar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<?php

$pagemaincontent = ob_get_contents();
ob_end_clean();

$pagetitle = "Funcionarios";

include("master.php");


?>