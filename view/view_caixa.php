<?php

  ob_start();
require_once("../model/conexao.php");
require_once("../model/model_funcao.php");
 $data = array();
 $comanda = null;
 $total = null;
 $quant = null;
 $result = null;
if ($_POST) {
  # code...

  $comanda = $_POST['nmComanda'];
  $status = "Aberto";
  //echo $comanda;

  $a = listar_pedidos($conexao, $comanda, $status);
//var_dump($a);
    # code...
    

    while($row = mysqli_fetch_array($a))
    {
          # code...
      $data[] = array("id_pedido_produto" => $row['id_pedido_produto'],"num_comanda" => $row['num_comanda'],"nome_produto" => $row['nome_produto'],"valor_unitario" => $row['valor_unitario'], "quantidade" => $row['quantidade']);
      $total += $row['valor_unitario'];
      $quant += $row['quantidade'];
    }

    $result = number_format($quant*$total, 2);;

  
}

if(@$_GET['status'] == 'sucesso')
{ ?>
  <div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="alert alert-success" role="alert">
        <strong>Comanda finalizada com sucesso!</strong>
      </div>
    </div>
  </div>

<?php
}

if(@$_GET['status'] == 'erro')
{ ?>
  <div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="alert alert-danger" role="alert">
        <strong>Ocorreu um erro, tente novamente!</strong>
      </div>
    </div>
  </div>
<?php
}

?>

<div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
         <div class="x_title">
            <h2>
               Caixa
            </h2>
            <div class="clearfix">
            </div>
         </div>
         <div class="title_right">
          <form action="" method="POST">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
               <div class="input-group">
                  <input type="text" class="form-control" name="nmComanda" placeholder="Comanda">
                  <span class="input-group-btn">
                     <button class="btn btn-default" type="submit" name="fmdBusca">
                        Procurar
                     </button>
                  </span>
               </div>
            </div>
           </form> 
         </div>
         <div class="x_content">
            <table class="table table-bordered">
               <thead>
                  <tr>
                     <td>
                        PRODUTO
                     </td>
                     <td>
                        QTD
                     </td>
                     <td>
                        VALOR
                     </td>
                  </tr> 
               </thead>
               <tbody>
                 <?php foreach ($data as $resultados) { ?>
                <tr>
                  <th><?=$resultados['nome_produto']?></th>
                  <th><?=$resultados['quantidade']?></th>
                  <th>R$<?=$resultados['valor_unitario']?></th>
                </tr>
                 <?php } ?>                
               </tbody>
            </table>
            <br>
            <form action="../control/baixa_comanda.php" method="POST">
                <input type="hidden" name="num_comanda" value="<?=@$data[0]['num_comanda']?>">
               <div class="form-group btn-group">
                   <div class="col-md-3 col-sm-3 col-xs-6">
                         <label for="Nome">
                     Desconto
                  </label>

                         <div class="input-group">
                          <div class="input-group-addon">R$</div>
                           <input type="text" name="desconto" class="form-control valor-desconto money" id="field" id="mask-moeda" value="0.00" required>                    
                            
                          </div>
                        </div>
                  <!-- <input type="text" class="form-control valor-desconto" id="nome" name="desconto" maxlength="10" value="0"> -->
               </div>
               <div class="form-group btn-group">
                   <div class="col-md-3 col-sm-3 col-xs-6">
                         <label for="Nome">
                      Sub Total
                  </label>

                         <div class="input-group">
                          <div class="input-group-addon">R$</div>
                           <input type="text" name="valor_total" readonly class="form-control valor-sub-total" id="field" id="mask-moeda" value="<?=$result?>" required>                    
                            
                          </div>
                        </div>

                  <!-- <input type="text" class="form-control valor-sub-total" id="nome" maxlength="10"  name="subTotal"
                  readonly> -->
               </div>
               <div class="form-group btn-group">
                  <div class="col-md-3 col-sm-3 col-xs-6">
                         <label for="Nome">
                      Valor Recebido
                  </label>
                         <div class="input-group">
                          <div class="input-group-addon">R$</div>
                           <input type="text" name="valor_recebido" class="form-control valor-recebido money" id="field" id="mask-moeda"  required>                    
                          </div>
                        </div>
               </div>
               <div class="form-group btn-group">
                  <div class="col-md-3 col-sm-3 col-xs-6">
                         <label for="Nome">
                     Troco
                  </label>
                         <div class="input-group">
                          <div class="input-group-addon">R$</div>
                           <input type="text" name="troco" readonly class="form-control valor-troco" id="field" id="mask-moeda"  required>                    
                          </div>
                        </div>
               </div>               
               <br>
               <div class="form-group btn-group">
                  <div class="col-md-3 col-sm-3 col-xs-6">
                     <label for="Nome">
                        Forma de pagamento
                    </label>
                     <div class="input-group">
                      <div class="input-group-addon"><i class="fa fa-exchange" aria-hidden="true"></i></div>
                       <select name="forma_pagamento" class="form-control">
                         <option value="dinheiro">Dinheiro</option>
                         <option value="cartao">Cartão</option>
                       </select>                    
                      </div>
                    </div>
               </div>
               <br />
               <div class="form-group">
                  <div class="col-md-6">
                      <?php if(@$data[0]['num_comanda'] == null){
                        ?><input type="submit" disabled value="Confirmar" class="btn btn-primary">  <?php 
                      }else
                      {
                        ?><input type="submit" value="Confirmar" class="btn btn-primary"> <?php 
                      } ?>
                                      
                  </div>
            </form>
            </div>
         </div>
      </div>
   </div>


<?php

$pagemaincontent = ob_get_contents();
ob_end_clean();

$pagetitle = "Funcionarios";

include("master.php");
?>