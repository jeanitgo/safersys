<?php

  ob_start();
require_once("../model/conexao.php");
require_once("../model/model_funcao.php");
if (@$_SESSION['perfil_id'] == 2 || @$_SESSION['perfil_id'] == 3) {
    # code...
    session_destroy();
  // Redireciona o visitante de volta pro login
    header("Location: ../index.php"); 
}
$b = listar_tipoProduto($conexao);

?>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>
                    Cadastrar Produto
                </h2>
                <div class="clearfix">
                </div>
            </div>
            <div class="x_content">
                <form class="form-horizontal form-label-left" action="..\control\cad_produto.php" enctype="multipart/form-data" novalidate method="POST"
                id="formProd" data-toggle="validator" role="form">
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                            Nome
                            <span class="required">
                                *
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" id="nome" name="nomeProduto" maxlength="45"
                            required>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">
                            Valor Unitario
                            <span class="required">
                                *
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">


                         <div class="input-group">
                          <div class="input-group-addon">R$</div>
                           <input type="text" name="valorUnitario" class="form-control" id="field" id="mask-moeda" onkeyup="maskMoeda(this.id);" required>                    
                            <div class="input-group-addon">.00</div>
                          </div>
                        </div>

                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">
                            Categoria Produto
                            <span class="required">
                                *
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control" name="categoria" id="" >
                              <option value="0">--Selecione--</option>
                              <?php foreach ($b as $key) { ?>
                                    <option value="<?=$key['id_categoria']?>"><?=$key['nome_categoria']?></option>
                              <?php }?>
                            </select>

                        </div>
                    </div>
                     <div class="item form-group">
                        <label for="password2" class="control-label col-md-3 col-sm-3 col-xs-12">
                            Selecione a imagem
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="imagem" type="file" name="imagem" class="form-control col-md-7 col-xs-12" required="required">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">
                            Descrição
                            <span class="required">

                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea class="form-control"name="descricao" id="" cols="30" rows="5"></textarea>
                        </div>
                    </div>

                    <div class="ln_solid">
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <input type="hidden" name="frmCadUsuario">
                            <button type="submit" class="btn btn-primary">
                                Adicionar
                            </button>
                            <button type="reset" class="btn btn-warning">
                                Limpar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<?php

$pagemaincontent = ob_get_contents();
ob_end_clean();

$pagetitle = "Produtos";

include("master.php");


?>
