function getMoney( str )
{
        return parseInt( str.replace(/[\D]+/g,'') );
}
function formatReal( int )
{ 
        var tmp = int+'';
        tmp = tmp.replace(/.([0-9]{2})$/g, ",$1");
        if( tmp.length > 6 )
                tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");

        return tmp;
}

$( document ).ready(function() {
	$('.money').mask('000.000.000.000.000,00', {reverse: true});

    $( ".valor-recebido" ).keyup(function() {
	  	console.log($(this).val());
	  	var valor_total = 0;

	  	if($('.valor-desconto').val() == "" || $('.valor-desconto').val() == null)
	  	{
	  		$('.valor-desconto').val('0,00');
	  	}

	  	sub_total = parseFloat($('.valor-sub-total').val()) - parseFloat($('.valor-desconto').val());
	  	valor_total = parseFloat($(this).val()) - sub_total; 

	  	valor_total = valor_total.toFixed(2);
	  	valor_total = formatReal(valor_total);

	  	if($('.valor-sub-total').val() != "" && $('.valor-sub-total').val() != null)
	  	{

		  	if($('.valor-sub-total').val() > parseFloat($(this).val()) || $(this).val() == "" || $(this).val() == null)
		  	{
		  		$('.valor-troco').val('0,00');
		  	}
		  	else
		  	{
		  		$('.valor-troco').val(valor_total);
		  	}
		}
		else
		{
			$('.valor-troco').val('0,00');
		}
	});

	$( ".valor-desconto" ).keyup(function() {
		
	  	valor_desconto = parseFloat($('.valor-sub-total').val()) - parseFloat($(this).val()); 
	  	valor_desconto = formatReal(valor_desconto);

	  	if($(this).val() != "" && $(this).val() != null)
	  	{
		  	if($('.valor-sub-total').val() != "" && $('.valor-sub-total').val() != null)
		  	{

			  	if($('.valor-recebido').val() != "" && parseFloat($('.valor-recebido').val()) > 0)
			  	{
			  		valor_troco = parseFloat($('.valor-recebido').val()) - parseFloat(valor_desconto);
			  		valor_desconto = parseFloat(valor_desconto);
			  		valor_desconto = valor_desconto.toFixed(2);

			  		$('.valor-troco').val(valor_troco.toFixed(2));
			  	}
			  	else
			  	{
			  		console.log($('.valor-recebido').val());
			  		$('.valor-troco').val('0,00');
			  	}
			}
			else
			{
				$('.valor-troco').val('0,00');
			}
		}
		else
		{
			if($('.valor-sub-total').val() != "" && $('.valor-sub-total').val() != null)
		  	{
			  	if($('.valor-recebido').val() != "" && parseFloat($('.valor-recebido').val()) > 0)
			  	{
			  		valor_troco = parseFloat($('.valor-recebido').val()) - parseFloat($('.valor-sub-total').val());
			  		valor_troco = parseFloat(valor_troco);
			  		$('.valor-troco').val(valor_troco.toFixed(2));
			  	}
			  	else
			  	{
			  		$('.valor-troco').val('0,00');
			  	}
			}
			else
			{
				$('.valor-troco').val('0,00');
			}
		}
	});
});