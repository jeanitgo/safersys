$(function(){
	$('#btnBuscar').click(function(){
		$.post('../control/listar_produtos.php',{categoria: $('#categoria').val()},function(data){
			obj = JSON.parse(data);
			$('.listarProdutos').html('');
			$.each( obj, function( key, value ) {
			  $('.listarProdutos').append('<div class="animated flipInY col-lg-2 col-md-3 col-sm-4 col-xs-12"><div class="tile-stats"><div class="right"><img  src="../imagens/'+value["foto"]+'" class="img-responsive img-thumbnail" width="100%" alt="Responsive image"><h4 name="nome_produto"align="center">'+value["nome_produto"]+'</h4><h5 name="valor_unitario"align="center">R$'+value["valor_unitario"]+'</h5><center><input type="number" class="teste" name="quantity" min="1" max="10"><div class="form-group"><input type="hidden" class="id" value="'+value["id_produto"]+'"><div class="btnAdicionar"><a href="#" class="btn btn-primary">Adicionar</a></div></div></center></div></div></div>');
			});
		});


	});



	$(document).on('click', '.btnAdicionar a', function(){
		var id = $(this).parents('.form-group').children('input').val();
		var qtd = $(this).parents('.form-group').parents('center').children('.teste').val();
		//alert(teste);
		$('#tblProduto tbody').append('<tr><td>'+id+'</td><td>'+qtd+'</td></tr>');
	});

	$(document).on('click', '#btnSalvar', function(){
		var table = $('table tbody');
		var dados = [];
		table.find('tr').each(function(key, indice){
			dados.push({'id': $(indice).find('td:eq(0)').text(), 'Qtd': $(indice).find('td:eq(1)').text()});
		    
		});
		
		$.post('../control/salva_pedido.php',
			{
				comanda: $('#Ncomanda').val(),
				pedidos: dados

			},function(data){
				console.log(data);
		});
	});
});